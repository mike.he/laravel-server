![Laravel](https://laravel.com/img/logotype.min.svg)

- 文档 [[Laravel Doc]](https://laravel.com/docs/5.8)
- 数据ORM文档 [[Laravel Doctrine ORM Doc]](https://www.laraveldoctrine.org/docs/1.3/orm)
- 数据迁移文档 [[Laravel Doctrine Migration Doc]](https://www.laraveldoctrine.org/docs/1.3/migrations)
- PHP代码修复工具 [[PHP Coding Standards Fixer]](https://cs.symfony.com/)