<?php

declare(strict_types=1);

namespace Database\Migrations;

use App\Entities\Scientist;
use App\Entities\Theory;
use Doctrine\DBAL\Schema\Schema as Schema;
use Doctrine\Migrations\AbstractMigration;

class Version20190822091533 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $em = app('em');
        $em->beginTransaction();

        $theory = new Theory();
        $theory->setTitle('My theory');

        $scientist = new Scientist();
        $scientist->setFirstname('Mike');
        $scientist->setLastname('He');
        $scientist->addTheory($theory);

        $em->persist($scientist);
        $em->flush();
        $em->commit();
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
    }
}
